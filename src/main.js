// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import { store } from './store'

import firebase from 'firebase'
// todo
// cssVars()

Vue.use(BootstrapVue)

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDbp8vpK71OIKpeOlS0_iR3P51_y36AqD4",
  authDomain: "vueproject-8d7b6.firebaseapp.com",
  databaseURL: "https://vueproject-8d7b6.firebaseio.com",
  projectId: "vueproject-8d7b6",
  storageBucket: "vueproject-8d7b6.appspot.com",
  messagingSenderId: "147470182363"
};
var defaultApp;
var defaultStorage;
var defaultDatabase;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  },
  created() {
    defaultApp = firebase.initializeApp(config);
    defaultStorage = defaultApp.storage();
    defaultDatabase = defaultApp.database();

    firebase.auth().onAuthStateChanged((user) => {
      if(user) {
        console.log(user);
        this.$store.state.email = user.email;
      } else {
        //this.$router.push('/auth')
      }
    });
    firebase.database().ref('users/1').set({
        age : "50",
        age_city : "50_Bucheon",
        city: 'Bucheon',
        email: 'neat507@naver.com',
        name: 'daehyun kim'
    })
  }
})
