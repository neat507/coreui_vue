import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
      id: 1,
      email: ""
    },
    getters: {
      getId: function(state) {
        return state.id;
      }
    },
    mutations: {
      setId: function(state, payload) {
        state.id = payload;
      }
    },
    actions: {
      setAsyncId: function(context, payload) {
        context.commit('setId', payload);
      }
    }
});
